package pjm.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * ConnectionFactory
 */
public class ConnectionFactory {
	// Docker config: docker run --rm --name pg-docker -e POSTGRES_PASSWORD=SuperSecret -d -p 5432:5432 postgres
    private static String url = "jdbc:postgresql://127.0.0.1:5432/";
	private static String database = "pjm";
	private static String driver = "org.postgresql.Driver";
	private static String user = "postgres";
	private static String password = "SuperSecret";

    /**
     * @TODO Make it possible to switch between databases
     */
     static Connection openConnection() {
        try {
			Class.forName(driver).newInstance();
			Connection connection = DriverManager.getConnection(url + database, user, password);
			return connection;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.err.println(
				"Something went wrong. Did you notice?");
		}
		return null; // Maybe use Optional?
     }
}