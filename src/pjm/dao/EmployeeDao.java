package pjm.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import pjm.model.Employee;

/**
 * EmployeeDao
 */
public class EmployeeDao implements Dao<Employee> {

    private static final String TABLE = "employee";

    private static final String INSERT = "INSERT INTO " + TABLE + " (employeeName, department) VALUES (?, ?)";

    private static final String UPDATE = "UPDATE " + TABLE + " SET employeeName = ?, department = ? WHERE id = ?";

    private static final String ALL = "SELECT * FROM " + TABLE;

    private static final String FIND_BY_ID = "SELECT * FROM " + TABLE + " WHERE id = ?";

    private static final String DELETE = "DELETE FROM " + TABLE + " WHERE id = ?";

    @Override
    public Optional<Employee> get(int id) {
        Connection c = ConnectionFactory.openConnection();
        Optional<Employee> employee = Optional.empty();

        try {
            PreparedStatement pstmt = c.prepareStatement(FIND_BY_ID);
            pstmt.setInt(1, id);

            ResultSet rset = pstmt.executeQuery();
            while (rset.next()) {
                employee = Optional.of(createEmployee(rset));
            }
            
            pstmt.close();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return employee;
    }

    @Override
    public List<Employee> getAll() {
        Connection c = ConnectionFactory.openConnection();
        ArrayList<Employee> employees = new ArrayList<Employee>();

        try {
            PreparedStatement pstmt = c.prepareStatement(ALL);
            ResultSet rset = pstmt.executeQuery();
            
            while (rset.next()) {
                employees.add(createEmployee(rset));
            }

            pstmt.close();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return employees;
    }

    @Override
    public void save(Employee employee) {
        Connection c = ConnectionFactory.openConnection();

        try {
            PreparedStatement pstmt = c.prepareStatement(INSERT);
            pstmt.setString(1, employee.getName());
            pstmt.setInt(2, employee.getDepartmentId());
            pstmt.executeUpdate();
            
            pstmt.close();
            c.close();
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Employee employee) {
        Connection c = ConnectionFactory.openConnection();

        try {
            PreparedStatement pstmt = c.prepareStatement(UPDATE);
            pstmt.setString(1, employee.getName());
            pstmt.setInt(2, employee.getDepartmentId());
            pstmt.setInt(3, employee.getId());
            pstmt.executeUpdate();

            pstmt.close();
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Employee employee) {
        Connection c = ConnectionFactory.openConnection();

        try {
            PreparedStatement pstmt = c.prepareStatement(DELETE);
            pstmt.setInt(1, employee.getId());

            pstmt.close();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    
    private Employee createEmployee(ResultSet rset) throws SQLException {
        Employee e = new Employee();

        e.setName(rset.getString("employeeName"));
        e.setDepartmentId(rset.getInt("department"));
        e.setId(rset.getInt("id"));

        return e;
    }

}