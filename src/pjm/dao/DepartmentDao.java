package pjm.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import pjm.model.Department;

/**
 * DepartmentDao
 */
public class DepartmentDao implements Dao<Department> {

    private static final String
    TABLE = "department";

    private static final String
    INSERT = "INSERT INTO " + TABLE + " (departmentName) VALUES (?)";
    
    private static final String
	UPDATE = "UPDATE " + TABLE + " SET departmentName = ? WHERE id = ?";

	private static final String
	ALL = "SELECT * FROM " + TABLE;
	
	private static final String
	FIND_BY_ID = "SELECT * FROM " + TABLE + " WHERE id = ?";

	private static final String 
	DELETE = "DELETE FROM " + TABLE + " WHERE id = ?";

    @Override
    public Optional<Department> get(int id) {
        Connection c = ConnectionFactory.openConnection();
        Optional<Department> department = Optional.empty();

        try {
            PreparedStatement pstmt = c.prepareStatement(FIND_BY_ID);
            pstmt.setInt(1, id);

            ResultSet rset = pstmt.executeQuery();
            while (rset.next()) {
                department = Optional.of(createDepartment(rset));
            }
            
            pstmt.close();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return department;
    }

    @Override
    public List<Department> getAll() {
        Connection c = ConnectionFactory.openConnection();
        ArrayList<Department> departments = new ArrayList<Department>();

        try {
            PreparedStatement pstmt = c.prepareStatement(ALL);
            ResultSet rset = pstmt.executeQuery();
            
            while (rset.next()) {
                departments.add(createDepartment(rset));
            }

            pstmt.close();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return departments;
    }

    @Override
    public void save(Department department) {
        Connection c = ConnectionFactory.openConnection();

        try {
            PreparedStatement pstmt = c.prepareStatement(INSERT);
            pstmt.setString(1, department.getDepartmentName());
            pstmt.executeUpdate();
            
            pstmt.close();
            c.close();
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Department department) {
        Connection c = ConnectionFactory.openConnection();

        try {
            PreparedStatement pstmt = c.prepareStatement(UPDATE);
            pstmt.setString(1, department.getDepartmentName());
            pstmt.setInt(2, department.getId());
            pstmt.executeUpdate();

            pstmt.close();
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Department department) {
        Connection c = ConnectionFactory.openConnection();

        try {
            PreparedStatement pstmt = c.prepareStatement(DELETE);
            pstmt.setInt(1, department.getId());

            pstmt.close();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Department createDepartment(ResultSet rset) throws SQLException {
        Department d = new Department();

        d.setDepartmentName(rset.getString("departmentName"));
        d.setId(rset.getInt("id"));

        return d;
    }    
}