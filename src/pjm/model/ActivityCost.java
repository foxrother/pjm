package pjm.model;

/**
 * ActivityCost
 */
public class ActivityCost {

    private int id;
    private String name;
    private String description;
    private String type;
    private float plannedCost;
    private float actualCost;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float getPlannedCost() {
        return plannedCost;
    }

    public void setPlannedCost(float plannedCost) {
        this.plannedCost = plannedCost;
    }

    public float getActualCost() {
        return actualCost;
    }

    public void setActualCost(float actualCost) {
        this.actualCost = actualCost;
    }
    
}