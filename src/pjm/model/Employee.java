package pjm.model;

/**
 * EmployeeModel
 */
public class Employee {

    private int id;
    private String name;
    private int departmentId;

    public Employee(String name, int departmentId) {
        this.departmentId = departmentId;
        this.name = name;
	}

	public Employee() {
	}

	public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    @Override
    public String toString() {
        return "Employee: " + this.name + " (" + this.id +  ")";
    }
}