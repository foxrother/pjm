package pjm.model;

/**
 * ActivityEmployee
 */
public class ActivityEmployee {
    private int activityId;
    private int employeeId;
    private float plannedEffort;
    private float actualEffort;
    private float hourlyRate;

    public int getActivityId() {
        return activityId;
    }

    public void setActivityId(int activityId) {
        this.activityId = activityId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public float getPlannedEffort() {
        return plannedEffort;
    }

    public void setPlannedEffort(float plannedEffort) {
        this.plannedEffort = plannedEffort;
    }

    public float getActualEffort() {
        return actualEffort;
    }

    public void setActualEffort(float actualEffort) {
        this.actualEffort = actualEffort;
    }

    public float getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(float hourlyRate) {
        this.hourlyRate = hourlyRate;
    }
}