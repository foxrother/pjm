package pjm.view;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

/**
 * Window
 */
public class Window {
    private JFrame jframe;
    private JMenuBar menuBar;
    private JPanel jpanel;

    public Window() {
        jframe = new JFrame("Project Management Application");
        jframe.setMinimumSize(new Dimension(900, 600));
        jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        menuBar = new JMenuBar();
        jframe.setJMenuBar(menuBar);
        JMenu menu = new JMenu("Menu");
        menuBar.add(menu);

        JMenuItem openDepartmentView = new JMenuItem("Departments");
        openDepartmentView.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jframe.setContentPane(new DepartmentView());
                jframe.revalidate();
            }
        });
        menu.add(openDepartmentView);
        
        JMenuItem openEmployeeView = new JMenuItem("Employees");
        openEmployeeView.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jframe.setContentPane(new EmployeeView());
                jframe.revalidate();
            }
        });
        menu.add(openEmployeeView);

        jpanel = new JPanel();
        jframe.setContentPane(jpanel);

        jframe.pack();
        jframe.setVisible(true);
    }
}