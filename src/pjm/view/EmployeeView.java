package pjm.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import net.miginfocom.layout.Grid;
import net.miginfocom.swing.MigLayout;
import pjm.dao.DepartmentDao;
import pjm.dao.EmployeeDao;
import pjm.model.Department;
import pjm.model.Employee;

/**
 * Department
 */
public class EmployeeView extends JPanel {

    private static final long serialVersionUID = 1L;
    private EmployeeDao employeeDao = new EmployeeDao();
    private List<Employee> employees;
    private JTextField employeeNameTextField;
    private DefaultListModel<Employee> employeeListModel = new DefaultListModel<Employee>();
    private JList<Employee> employeeJList = new JList<>(employeeListModel);
    private EmployeeJListListener jListListener = new EmployeeJListListener();

    private DepartmentDao departmentDao = new DepartmentDao();
    private List<Department> departments;
    private DefaultListModel<Department> departmentListModel = new DefaultListModel<Department>();
    private JList<Department> departmentJList = new JList<Department>(departmentListModel);

    public EmployeeView() {
        super(new MigLayout("top, left, gap 10"));

        JScrollPane employeeJScrollPane = new JScrollPane(employeeJList);
        add(employeeJScrollPane, "dock west, wmin 300, hmin 100%");

        JScrollPane departmentJScrollPane = new JScrollPane(departmentJList);
        add(departmentJScrollPane, "dock east");

        add(new JLabel("Selected department's name:"), "span, wrap 0");
        employeeNameTextField = new JTextField();
        add(employeeNameTextField, "wmin 200, wrap");

        
        JButton addRecord = new JButton("Add employee");
        addRecord.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Employee employee = new Employee(
                    employeeNameTextField.getText(),
                    departmentJList.getSelectedValue().getId()
                );
                employeeDao.save(employee);
            }
        });
        add(addRecord);
        
        JButton updateRecord = new JButton("Update employee");
        updateRecord.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                Employee employee = employeeJList.getSelectedValue();
                employee.setName(employeeNameTextField.getText());
                employeeDao.update(employee);
                employeeJList.getSelectedValue().setName(employeeNameTextField.getText());
                employeeJList.repaint();
            }
        });
        add(updateRecord, "wrap");

        JButton fetchAll = new JButton("Fetch all employees");
        fetchAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                employees = employeeDao.getAll();
                employeeJList.removeListSelectionListener(jListListener);
                employeeJList.clearSelection();
                employeeListModel.clear();
                for (Employee employee : employees) {
                    employeeListModel.addElement(employee);
                }
                employeeJList.addListSelectionListener(jListListener);
            }
        });
        add(fetchAll);
        
        departments = departmentDao.getAll();
        for (Department department : departments) {
            departmentListModel.addElement(department);
        }

        setVisible(true);
    }

    public class EmployeeJListListener implements ListSelectionListener {
        public void valueChanged(ListSelectionEvent e) {
            if (!employeeJList.getValueIsAdjusting() && !employeeJList.isSelectionEmpty());
                employeeNameTextField.setText(employeeJList.getSelectedValue().getName());
        }
    }
}