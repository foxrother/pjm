package pjm.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import net.miginfocom.layout.Grid;
import net.miginfocom.swing.MigLayout;

import pjm.dao.DepartmentDao;
import pjm.model.Department;

/**
 * Department
 */
public class DepartmentView extends JPanel {

    private static final long serialVersionUID = 1L;
    private DepartmentDao departmentDao = new DepartmentDao();
    private List<Department> departments;
    private JTextField departmentNameTextField;
    private DefaultListModel<Department> departmentListModel = new DefaultListModel<Department>();
    private JList<Department> departmentJList = new JList<>(departmentListModel);
    private DepartmentJListListener jListListener = new DepartmentJListListener();

    public DepartmentView() {
        super(new MigLayout("top, left, gap 10"));

        JScrollPane departmentJScrollPane = new JScrollPane(departmentJList);
        add(departmentJScrollPane, "dock west, wmin 300, hmin 100%");

        add(new JLabel("Selected department's name:"), "span, wrap 0");
        departmentNameTextField = new JTextField();
        add(departmentNameTextField, "wmin 200, wrap");

        
        JButton addRecord = new JButton("Add department");
        addRecord.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Department department = new Department(departmentNameTextField.getText());
                departmentDao.save(department);
            }
        });
        add(addRecord);
        
        JButton updateRecord = new JButton("Update department");
        updateRecord.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Department department = departmentJList.getSelectedValue();
                department.setDepartmentName(departmentNameTextField.getText());
                departmentDao.update(department);
                departmentJList.getSelectedValue().setDepartmentName(departmentNameTextField.getText());
                departmentJList.repaint();
            }
        });
        add(updateRecord, "wrap");

        JButton fetchAll = new JButton("Fetch all departments");
        fetchAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                departments = departmentDao.getAll();
                departmentJList.removeListSelectionListener(jListListener);
                departmentJList.clearSelection();
                departmentListModel.clear();
                for (Department department : departments) {
                    departmentListModel.addElement(department);
                }
                departmentJList.addListSelectionListener(jListListener);
            }
        });
        add(fetchAll);
        
        setVisible(true);
    }

    public class DepartmentJListListener implements ListSelectionListener {
        public void valueChanged(ListSelectionEvent e) {
            if (!departmentJList.getValueIsAdjusting() && !departmentJList.isSelectionEmpty());
                departmentNameTextField.setText(departmentJList.getSelectedValue().getDepartmentName());
        }
    }
}