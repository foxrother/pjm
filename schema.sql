-- Docker config: docker run --rm --name pg-docker -e POSTGRES_PASSWORD=SuperSecret -d -p 5432:5432 postgres
-- Department, Employee and HourlyRate --------------------
CREATE TABLE Department (
    id SERIAL PRIMARY KEY,
    departmentName varchar(240) NOT NULL
);

CREATE TABLE HourlyRate (
    id SERIAL PRIMARY KEY,
    hourlyRate money NOT NULL,
    description varchar(240) NOT NULL,
    code varchar(48) NOT NULL
);

CREATE TABLE Employee (
    id SERIAL PRIMARY KEY,
    employeeName varchar(240) NOT NULL,
    department int REFERENCES Department(id)
);

-- Portfolio, Program and Project -------------------------
CREATE TABLE Portfolio (
    id SERIAL PRIMARY KEY,
    portfolioName varchar(240) NOT NULL,
    description varchar(2048),
    manager int REFERENCES Employee(id)
);

CREATE TABLE Program (
    id SERIAL PRIMARY KEY,
    programName varchar(240),
    description varchar(2048),
    manager int REFERENCES Employee(id),
    portfolio int REFERENCES Portfolio(id)
);

CREATE TABLE Project (
    id SERIAL PRIMARY KEY,
    projectName varchar(240) NOT NULL,
    description varchar(4096),
    budget money NOT NULL,
    dueDate timestamp NOT NULL,
    completionDate timestamp,
    manager int REFERENCES Employee(id),
    program int REFERENCES Program(id)
);

-- Activity and cost-related tables -----------------------
CREATE TABLE Activity (
    id SERIAL PRIMARY KEY,
    project int REFERENCES Project(id),
    name varchar(240) NOT NULL,
    description varchar(2048),
    dueDate timestamp NOT NULL,
    completionDate timestamp
);

CREATE TABLE ActivityDependency (
    activity int REFERENCES Activity(id),
    dependency int REFERENCES Activity(id),
    PRIMARY KEY (activity, dependency)
);

CREATE TABLE CostType (
    id SERIAL PRIMARY KEY,
    costName varchar(120) NOT NULL,
    description varchar(1024) NOT NULL
);

CREATE TABLE ActivityCost (
    id SERIAL PRIMARY KEY,
    activity int REFERENCES Activity(id),
    activityCostName varchar(512) NOT NULL,
    description varchar(512),
    type int REFERENCES CostType(id),
    plannedCost money NOT NULL,
    actualCost money NOT NULL
);
  
CREATE TABLE ActivityEmployee (
    activity int REFERENCES Activity(id),
    employee int REFERENCES Employee(id),
    plannedEffort float NOT NULL,
    actualEffort float NOT NULL,
    hourlyRate int REFERENCES HourlyRate(id),
    PRIMARY KEY (activity, employee)
);